# 
# SiSH - the Simple Shell
# Copyright (C) 2020 0v3rl0w & contributors
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
# 


include config.mk

SRC = sish.c
OBJ = ${SRC:.c=.o}

all: options sish

options:
	@echo SiSH build options:
	@echo "CFLAGS  = ${CFLAGS}"
	@echo "CC      = ${CC}"

.c.o:
	${CC} -c ${CFLAGS} $<

${OBJ}: config.h config.mk

config.h:
	cp config.def.h $@

sish: ${OBJ}
	${CC} -o $@ ${OBJ}

clean:
	rm -f sish ${OBJ}

install: all
	mkdir -p ${DESTDIR}${PREFIX}/bin
	cp -f dwm ${DESTDIR}${PREFIX}/bin
	chmod 755 ${DESTDIR}${PREFIX}/bin/sish

uninstall:
	rm -f ${DESTDIR}${PREFIX}/bin/sish

.PHONY: all options clean install uninstall
