/*
 * SiSH - The Simple Shell
 * Copyright (C) 2020 0v3rl0w & contributors 
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <pwd.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h> 

#define _POSIX_C_SOURCE 200809L
#define _DEFAULT_BUFFSIZE 1024
#define _DELIM_TOKEN " \t\r\n\a"

static void sish_loop(void);
static void sish_ctrlc(void);
static int sish_execute(char *line);
static char *sish_input(void);
static int sish_cd(char **args);
static int sish_exit(char **args);
static char *PS1_parser(void);
static char *PS1_info_by_letter(char letter);
static void minify_path(char *path);

typedef struct SISH_TOKENS {
	char **content;
	int length;
} Token;

static Token splitline(char *line);
static void replacevar(Token *tokens);
static void destroytoken(Token *tokens);

#include "config.h"

char *builtin[] = {
	"cd",
	"exit"
};

int (*builtinfunc[]) (char **) = {
	&sish_cd,
	&sish_exit
};

int
sish_exit(char **args)
{
	exit(0);
}

int
sish_cd(char **args) 
{
	if(args[1] == NULL) 
		chdir(getenv("HOME"));
	else {
		if(chdir(args[1]) != 0) 
			fprintf(stderr, "cd: couldn't change directory\n");
	}

	return 1;
}

void 
destroytoken(Token *tokens)
{
	free(tokens->content);
	tokens = NULL;
}

Token
splitline(char *line)
{
	int buffsize = _DEFAULT_BUFFSIZE;
	int index = 0;

	char **tokens = malloc(sizeof(char *) * buffsize);

	if(tokens == NULL) {
		fprintf(stderr, "SiSH: couldn't allocate memory\n");
		exit(1);
	}

	char *token = strtok(line, _DELIM_TOKEN);

	while(token != NULL) {
		tokens[index++] = token;
		
		if(index >= buffsize) {
			buffsize += _DEFAULT_BUFFSIZE;
			token = realloc(token, buffsize * sizeof(char *));

			if(token == NULL) {
				fprintf(stderr, "SiSH: couldn't allocate memory\n");
				exit(1);
			}
		}

		token = strtok(NULL, _DELIM_TOKEN);
	}

	tokens[index] = '\0';

	Token returnvalue;
	returnvalue.content = tokens;
	returnvalue.length = index;

	return returnvalue;
}

void
replacevar(Token *tokens)
{
	int i;
	char *varname;

	for(i = 0; i < tokens->length; i++) {
		if(tokens->content[i][0] == '$') {
			varname = malloc(sizeof(char) * strlen(tokens->content[i]) - 1);
			
			if(varname == NULL) {
				fprintf(stderr, "SiSH: couldn't allocate memory\n");
				exit(1);
			}

			strncpy(varname, tokens->content[i]+1, strlen(tokens->content[i]));
			
			tokens->content[i] = getenv(varname);
			
			free(varname);
		}
	}
}

int
sish_execute(char *line)
{
	Token tokens = splitline(line);
	replacevar(&tokens);

	int i;
	int returncode = 0;

	for(i = 0; i < sizeof(builtin) / sizeof(char *); i++) {
		if(strcmp(tokens.content[0], builtin[i]) == 0) {
			returncode = (*builtinfunc[i])(tokens.content);
			destroytoken(&tokens);
			return returncode;
		}
	}

	int pid = fork();

	if(pid == 0) {
		if(execvp(tokens.content[0], tokens.content) == -1) {
			fprintf(stderr, "SiSH: command not found: %s\n", tokens.content[0]);
			returncode = 1;
			exit(1);
		}
	} else if(pid < 0) {
		fprintf(stderr, "SiSH: error while forking the process\n");
		exit(1);
	} else {
		int wpid;
		int status;

		do {
			wpid = waitpid(pid, &status, WUNTRACED);
		} while(!WIFEXITED(status) && !WIFSIGNALED(status));
	}
	
	destroytoken(&tokens);
	return returncode;
}

char *
sish_input(void)
{
	int buffsize = _DEFAULT_BUFFSIZE;
	char *buffer = malloc(sizeof(char) * buffsize);	
	int index = 0;
	char c;	

	if(buffer == NULL) {
		fprintf(stderr, "SiSH: couldn't allocate memory\n");
		exit(1);
	}

	for(;;) {
		c = getchar();

		if(c == EOF || c == '\n') {
			buffer[index] = '\0';
			return buffer;
		} else {
			buffer[index++] = c;
		}

		if(index >= buffsize) {
			buffsize += _DEFAULT_BUFFSIZE;
			buffer = realloc(buffer, sizeof(char) * buffsize);

			if(buffer == NULL) {
				fprintf(stderr, "\nSiSH: couldn't allocate memory\n");
				free(buffer);
				exit(1);
			}
		}
	}
}

void 
minify_path(char *path)
{
	char *buf = malloc(sizeof(char) * 1024);

	if(buf == NULL) {
		fprintf(stderr, "Couldn't allocate memory\n");
		exit(1);
	}

	struct passwd *pw = getpwuid(getuid());
	char *homepath = pw->pw_dir;


	if(strlen(homepath) > strlen(path)) {
		free(buf);
		return;
	}

	strncpy(buf, path, strlen(homepath));

	if(strncmp(buf, homepath, strlen(homepath)) == 0) {
		strncpy(buf, path+strlen(homepath), strlen(path));
		strcpy(path, "~");
		strcat(path, buf);
	}
	return;
}

char *
PS1_info_by_letter(char letter)
{
	char *return_value = malloc(sizeof(char) * 256);

	if(return_value == NULL) {
		fprintf(stderr, "SiSH: couldn't allocate memory\n");
		exit(1);
	}

	switch(letter) {
    case 'h':
		if(gethostname(return_value, 256) == -1) {
			fprintf(stderr, "SiSH: couldn't get the hostname\n");
			free(return_value);
			exit(1);
		}

		break;
    case 'u':
		if(getlogin_r(return_value, 256) != 0) {
			fprintf(stderr, "SiSH: couldn't get the username\n");
			free(return_value);
			exit(1);
		}
		break;
    case 'w':
		if(getcwd(return_value, 256) == NULL) {
			fprintf(stderr, "SiSH: couldn't get the current working directory\n");
			free(return_value);
			exit(1);
		}

		minify_path(return_value);
		break;

	case 'W':
		if(getcwd(return_value, 256) == NULL) {
			fprintf(stderr, "SiSH: couldn't get the current working directory\n");
			free(return_value);
			exit(1);
		}

		int last_slash = 0;
		int i;

		for(i=0; i < strlen(return_value); i++) 
			if(return_value[i] == '/')
				last_slash = i;

		char *buffer = malloc(sizeof(char) * strlen(return_value) - last_slash);
		strncpy(buffer, return_value+last_slash+1, strlen(return_value));
		strcpy(return_value, buffer);
		break;

	case '$':
		if(getuid() == 0)
			strcpy(return_value, "#");
		else
			strcpy(return_value, "$");
		break;

	default:
		fprintf(stderr, "SiSH: the option \\%c is not available for the PS1", letter);
		free(return_value);
		exit(1);
	}

	strcat(return_value, "\0");
	return return_value;
}

char * 
PS1_parser(void)
{
	int i;

	char *PS1_parsed = malloc(sizeof(char) * 1024);
	for(i = 0; i < strlen(PS1); i++) {
		if(PS1[i] == '\\') {
			char *toappend = PS1_info_by_letter(PS1[++i]);
			strncat(PS1_parsed, toappend, strlen(toappend));
			free(toappend);
		} else {
			strncat(PS1_parsed, &PS1[i], 1);
		}
	}	

	strcat(PS1_parsed, "\0");
	return PS1_parsed;
}

void
sish_loop(void)
{
	char *line;
	int returncode;

	for(;;) {
		char *PS1_parsed = PS1_parser();
		printf("%s", PS1_parsed);	
		line = sish_input();

		if(strlen(line) > 1)
			returncode = sish_execute(line);

		free(line);
		free(PS1_parsed);
	}

}	

void
sish_ctrlc(void)
{
	printf("\n");
	sish_loop();
}

int 
main(int argc, char *argv[])
{
	if(argc == 2) {
		if(strcmp(argv[1], "-v")  == 0){
			printf("SiSH %s\n", VERSION);
			exit(0);
		}
	}
	
	signal(SIGINT, sish_ctrlc);
	sish_loop();

	return 0;
}
